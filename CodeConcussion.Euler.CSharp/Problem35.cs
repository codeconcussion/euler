﻿using System.Collections.Generic;
using System.Globalization;
using Math = CodeConcussion.Euler.CSharp.Utility.Math;

namespace CodeConcussion.Euler.CSharp
{
    public sealed class Problem35 : IProblem<long>
    {
        public string Description =>
            @"The number, 197, is called a circular prime because all rotations of the digits: 197, 971, and 719, are themselves prime.

            There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, and 97.

            How many circular primes are there below one million?";

        public long Answer => 55;

        public long CalculatedAnswer { get; private set; }

        public void Solve()
        {
            var circularPrimes = new List<int>();

            for (var i = 2; i < 1000000; i++)
            {
                if (IsCircularPrime(i)) circularPrimes.Add(i);
            }

            CalculatedAnswer = circularPrimes.Count;
        }

        private static bool IsCircularPrime(int number)
        {
            return Math.IsPrime(number) && AreRotationsPrime(number);
        }

        private static bool AreRotationsPrime(int number)
        {
            var x = number.ToString(CultureInfo.InvariantCulture);
            
            for (var i = 0; i < (x.Length - 1); i++)
            {
                x = x.Substring(1, x.Length - 1) + x[0];
                if (!Math.IsPrime(int.Parse(x))) return false;
            }

            return true;
        }
    }
}