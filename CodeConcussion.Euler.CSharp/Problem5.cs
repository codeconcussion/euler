﻿using System.Collections.Generic;
using System.Linq;

namespace CodeConcussion.Euler.CSharp
{
    public sealed class Problem5 : IProblem<int>
    {
        public string Description => "Find the smallest positive number that is evenly divisible the numbers 1 through 20.";

        public int Answer => 232792560;

        public int CalculatedAnswer { get; private set; }

        public void Solve()
        {
            var range = Enumerable.Range(1, 20).ToList();
            var step = range.Max();
            var current = step;
            while (!IsDivisibleBy(current, range)) current += step;
            CalculatedAnswer = current;
        }

        private static bool IsDivisibleBy(int value, IEnumerable<int> range)
        {
            return range.All(x => IsDivisibleBy(value, x));
        }

        private static bool IsDivisibleBy(int dividend, int divisor)
        {
            return (dividend % divisor) == 0;
        }
    }
}