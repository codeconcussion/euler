﻿namespace CodeConcussion.Euler.CSharp
{
    public sealed class Problem7 : IProblem<long>
    {
        public string Description => "What is the 10,001st prime number?";

        public long Answer => 104743;

        public long CalculatedAnswer { get; private set; }

        public void Solve()
        {
            var current = 0;
            var count = 0;
            const int find = 10001;

            for(;;)
            {
                current++;
                if (!Utility.Math.IsPrime(current)) continue;

                count++;
                if (count < find) continue;

                break;
            }

            CalculatedAnswer = current;
        }
    }
}