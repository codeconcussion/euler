﻿using System.Globalization;
using System.Linq;

namespace CodeConcussion.Euler.CSharp
{
    public sealed class Problem4 : IProblem<int>
    {
        public string Description => "Find the largest palindrome made from the product of two 3-digit numbers.";

        public int Answer => 906609;

        public int CalculatedAnswer { get; private set; }

        public void Solve()
        {
            CalculatedAnswer = FindLargestPalindrome(100, 999);
        }

        private static int FindLargestPalindrome(int min, int max)
        {
            var largest = 0;
            var factorSum = 0;

            for (var x = max; x >= min; x--)
            {
                for (var y = max; y >= x; y--)
                {
                    var product = x * y;
                    if (IsPalindrome(product))
                    {
                        if (product > largest)
                        {
                            largest = product;
                            factorSum = x + y;
                        }
                        break;
                    }
                }

                //exit early if there's no possibilty of a higher palindrome
                //?!: not sure this is valid!
                if ((x + max) < factorSum) break;
            }

            return largest;
        }

        private static bool IsPalindrome(int value)
        {
            var check = value.ToString(CultureInfo.InvariantCulture);
            return Enumerable.SequenceEqual(check, check.Reverse());
        }
    }
}