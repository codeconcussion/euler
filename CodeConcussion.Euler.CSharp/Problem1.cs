﻿using System.Collections.Generic;
using System.Linq;

namespace CodeConcussion.Euler.CSharp
{
    public sealed class Problem1 : IProblem<int>
    {
        public string Description => "Find the sum of all positive integers below 1000 that are divisiable by 3 or 5.";

        public int Answer => 233168;

        public int CalculatedAnswer { get; private set; }
        
        public void Solve()
        {
            const int max = 999;
            var divisibleBy3 = MultiplesOf(3, max);
            var divisibleBy5 = MultiplesOf(5, max);
            CalculatedAnswer = Enumerable.Union(divisibleBy3, divisibleBy5).Sum();
        }
        
        private static IEnumerable<int> MultiplesOf(int value, int max)
        {
            var numberOfMultiples = max / value;
            var range = Enumerable.Range(1, numberOfMultiples);
            var multiples = range.Select(x => x * value);
            return multiples;
        }
    }
}