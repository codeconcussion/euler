﻿using System.Linq;
using Math = CodeConcussion.Euler.CSharp.Utility.Math;

namespace CodeConcussion.Euler.CSharp
{
    public sealed class Problem20 : IProblem<long>
    {
        public string Description =>
            @"n! means n × (n − 1) × ... × 3 × 2 × 1

            For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
            and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.

            Find the sum of the digits in the number 100!";

        public long Answer => 648;

        public long CalculatedAnswer { get; private set; }

        public void Solve()
        {
            var factorial = Math.StringFactorial(100);
            CalculatedAnswer = factorial.Aggregate(0, (x, y) => x + (int)char.GetNumericValue(y));
        }
    }
}