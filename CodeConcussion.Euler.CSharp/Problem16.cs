﻿using System.Globalization;
using System.Linq;

namespace CodeConcussion.Euler.CSharp
{
    public sealed class Problem16 : IProblem<long>
    {
        public string Description =>
            @"2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.

            What is the sum of the digits of the number 2^1000?";

        public long Answer => 1366;

        public long CalculatedAnswer { get; private set; }

        public void Solve()
        {
            CalculatedAnswer = SumOfDigits(StringPow(2, 1000));
        }

        private static long SumOfDigits(string number)
        {
            return number.Sum(x => (int) char.GetNumericValue(x));
        }

        private static string StringPow(int number, int pow)
        {
            var answer = "1";

            for (var i = 1; i <= pow; i++)
            {
                answer = Utility.Math.StringMultiply(number.ToString(CultureInfo.InvariantCulture), answer);
            }

            return answer;
        }
    }
}