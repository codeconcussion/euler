﻿using System.Collections.Generic;
using System.Linq;

namespace CodeConcussion.Euler.CSharp
{
    public sealed class Problem2 : IProblem<long>
    {
        public string Description => "Find the sum of even-valued items from the Fibonacci sequence up to four million";

        public long Answer => 4613732;

        public long CalculatedAnswer { get; private set; }

        public void Solve()
        {
            const int max = 4000000;
            var sequence = Fibonacci().TakeWhile(x => x <= max);
            var evens = sequence.Where(x => x % 2 == 0);
            CalculatedAnswer = evens.Sum();
        }

        public static IEnumerable<long> Fibonacci()
        {
            var current = 0L;
            var next = 1L;

            for(;;)
            {
                yield return current;
                var newCurrent = next;
                next = current + next;
                current = newCurrent;
            }
        }
    }
}
