﻿using System.Collections.Generic;
using System.Linq;

namespace CodeConcussion.Euler.CSharp
{
    public sealed class Problem17 : IProblem<long>
    {
        public string Description =>
            @"If the numbers 1 to 5 are written out in words: one, two, three, four, five,
            then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.

            If all the numbers from 1 to 1000 (one thousand) inclusive were written out
            in words, how many letters would be used?";

        public long Answer => 21124;

        public long CalculatedAnswer { get; private set; }

        public void Solve()
        {
            CalculatedAnswer = Enumerable.Range(0, 1001).Aggregate((x, y) => x + NumberToString(y).Count());
        }

        private static string NumberToString(int number)
        {
            if (number <= 19) return NumberMap[number];

            if (number <= 99)
            {
                if (number % 10 == 0) return NumberMap[number];
                return NumberMap[number / 10 * 10] + NumberMap[number % 10];
            }

            if (number <= 999)
            {
                var hundred = NumberMap[number / 100] + "hundred";
                if (number % 100 == 0) return hundred;
                return hundred + "and" + NumberToString(number % 100);
            }

            if (number <= 9999)
            {
                var thousand = NumberMap[number / 1000] + "thousand";
                var remainder = number % 1000;
                if (remainder == 0) return thousand;
                if (remainder <= 99) thousand += "and";
                return thousand + NumberToString(remainder);
            }

            return "";
        }

        private static readonly Dictionary<int, string> NumberMap = new Dictionary<int, string>
        {
            {1, "one"},
            {2, "two"},
            {3, "three"},
            {4, "four"},
            {5, "five"},
            {6, "six"},
            {7, "seven"},
            {8, "eight"},
            {9, "nine"},
            {10, "ten"},
            {11, "eleven"},
            {12, "twelve"},
            {13, "thirteen"},
            {14, "fourteen"},
            {15, "fifteen"},
            {16, "sixteen"},
            {17, "seventeen"},
            {18, "eighteen"},
            {19, "nineteen"},
            {20, "twenty"},
            {30, "thirty"},
            {40, "forty"},
            {50, "fifty"},
            {60, "sixty"},
            {70, "seventy"},
            {80, "eighty"},
            {90, "ninety"}
        };
    }
}