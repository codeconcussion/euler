﻿namespace CodeConcussion.Euler.CSharp
{
    public interface IProblem<out T>
    {
        T Answer { get; }
        T CalculatedAnswer { get; }
        string Description { get; }
        void Solve();
    }
}
