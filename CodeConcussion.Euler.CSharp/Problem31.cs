﻿using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Net.Mail;

namespace CodeConcussion.Euler.CSharp
{
    public sealed class Problem31 : IProblem<int>
    {
        public string Description =>
            @"In England the currency is made up of pound, £, and pence, p, and there are eight coins in general circulation:
            1p, 2p, 5p, 10p, 20p, 50p, £1 (100p) and £2 (200p).

            It is possible to make £2 in the following way:
            1×£1 + 1×50p + 2×20p + 1×5p + 1×2p + 3×1p

            How many different ways can £2 be made using any number of coins?";

        public int Answer => 73682;

        public int CalculatedAnswer { get; private set; }

        public void Solve()
        {
            const int target = 200;
            var denominations = new[] { 200, 100, 50, 20, 10, 5, 2, 1 };
            CalculatedAnswer = Solve(target, denominations);
        }

        private static int Solve(int target, int[] denominations)
        {
            if (denominations.Count() == 1) return 1;

            var largestDenomination = denominations.Max();
            var largestCount = target / largestDenomination;
            var sum = 0;

            for (var i = largestCount; i >= 0; i--)
            {
                var remaining = target - (i * largestDenomination);

                if (remaining == 0)
                {
                    sum++;
                }
                else
                {
                    sum += Solve(remaining, denominations.Except(new[] { largestDenomination }).ToArray());
                }
            }

            return sum;
        }
    }
}