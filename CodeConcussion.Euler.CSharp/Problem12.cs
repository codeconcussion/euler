﻿using System.Collections.Generic;
using System.Linq;
using Math = CodeConcussion.Euler.CSharp.Utility.Math;

namespace CodeConcussion.Euler.CSharp
{
    public sealed class Problem12 : IProblem<long>
    {
        public string Description =>
            @"A triangle number is sums of consecutive integers.
            The 7th triangle number is 28. 1+2+3+4+5+6+7=28.
            Find the first triangle number with 100 factors.";

        public long Answer => 76576500;

        public long CalculatedAnswer { get; private set; }

        public void Solve()
        {
            var numbers = GetTriangleNumbers();
            CalculatedAnswer = numbers.First(x => Math.Factors(x).Count() >= 500);
        }

        private static IEnumerable<long> GetTriangleNumbers()
        {
            var next = 0;
            for (var i = 1; i > 0; i++)
            {
                yield return next += i;
            }
        }
    }
}