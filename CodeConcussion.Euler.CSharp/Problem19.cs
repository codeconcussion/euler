﻿using System;
using System.Linq;

namespace CodeConcussion.Euler.CSharp
{
    public sealed class Problem19 : IProblem<long>
    {
        public string Description =>
            @"You are given the following information, but you may prefer to do some research for yourself.

            1 Jan 1900 was a Monday.
            A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.

            How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?";

        public long Answer => 171;

        public long CalculatedAnswer { get; private set; }

        public void Solve()
        {
            CalculatedAnswer = FindDays(1901, 2000, Day.Sunday, 1);
        }

        private int FindDays(int startYear, int endYear, Day day, int dayOfMonth)
        {
            return Enumerable
                .Range(startYear, endYear - startYear + 1)
                .SelectMany(x => Enumerable.Range(1, 12).Select(y => new DateTime(x, y, dayOfMonth)))
                .Select(GetDayOfWeek)
                .Count(x => x == day);
        }

        private Day GetDayOfWeek(DateTime date)
        {
            var difference = date.Subtract(_firstDate).Days;
            return (Day)(difference % 7);
        }

        private readonly DateTime _firstDate = new DateTime(1900, 1, 1);

        private enum Day
        {
            Monday,
            Tuesday,
            Wednesday,
            Thursday,
            Friday,
            Saturday,
            Sunday
        }
    }
}