﻿using System.Collections.Generic;
using System.Linq;
using Math = CodeConcussion.Euler.CSharp.Utility.Math;

namespace CodeConcussion.Euler.CSharp
{
    public sealed class Problem49 : IProblem<string>
    {
        public string Description =>
            @"The arithmetic sequence, 1487, 4817, 8147, in which each of the terms increases by 3330, is unusual in two ways: (i) each of the three terms are prime, and, (ii) each of the 4-digit numbers are permutations of one another.

            There are no arithmetic sequences made up of three 1-, 2-, or 3-digit primes, exhibiting this property, but there is one other 4-digit increasing sequence.

            What 12-digit number do you form by concatenating the three terms in this sequence?";

        public string Answer => "296962999629";

        public string CalculatedAnswer { get; private set; }

        public void Solve()
        {
            var primes = GetRelevantPrimes();
            var sameDigitGroups = GetPrimesWithSameDigits(primes);
            var match = sameDigitGroups.Select(FindMatch).First(x => x != null);
            var combined = string.Join("", match.Select(x => x.ToString()));
            CalculatedAnswer = combined;
        }

        private static List<List<int>> GetPrimesWithSameDigits(List<int> primes)
        {
            var groupedByDigits = primes.ToLookup(x => string.Concat(x.ToString().OrderBy(y => y)));
            var result = groupedByDigits.Where(x => x.Count() >= 3).Select(x => x.ToList());
            return result.ToList();
        }

        private static List<int> GetRelevantPrimes()
        {
            var fourDigitPrimes = Enumerable.Range(1001, 8994).Where(x => Math.IsPrime(x)).ToList();
            var existingSolution = new List<int> {1487, 4817, 8147};
            return fourDigitPrimes.Except(existingSolution).ToList();
        }

        private static List<int> FindMatch(List<int> source)
        {
            for (var x = 0; x < source.Count; x++)
            for (var y = (x + 1); y < source.Count; y++)
            {
                var difference = source[y] - source[x];
                var find = source[y] + difference;
                if (source.Contains(find)) return new List<int> { source[x], source[y], find };
            }

            return null;
        }
    }
}