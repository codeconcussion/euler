﻿using System;
using System.Linq;

namespace CodeConcussion.Euler.CSharp
{
    public sealed class Problem14 : IProblem<long>
    {
        public string Description =>
            @"The following iterative sequence is defined for the set of positive integers -
                n → n/2 (n is even)
                n → 3n + 1 (n is odd)

            Using the rule above and starting with 13, we generate the following sequence:
                13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1

            Which starting number, under one million, produces the longest chain?";

        public long Answer => 837799;

        public long CalculatedAnswer { get; private set; }

        public void Solve()
        {
            var answers = Enumerable.Range(1, Max).Select(x => Tuple.Create(x, Reduce(x))).ToList();
            var max = answers.Aggregate((x, y) => x.Item2 > y.Item2 ? x : y);
            CalculatedAnswer = max.Item1;
        }

        private static int Reduce(long original)
        {
            var count = 0;
            var current = original;

            while (true)
            {
                count++;

                if (current == 1)
                {
                    Cache[original - 1] = count;
                    return count;
                }

                if (current < Max && Cache[current - 1] > 0)
                {
                    count += Cache[current - 1];
                    Cache[original - 1] = count;
                    return count;
                }

                if (current % 2 == 0)
                {
                    current = current / 2;
                }
                else
                {
                    current = 3 * current + 1;
                }
            }
        }

        private const int Max = 999999;
        private static readonly int[] Cache = new int[Max];
    }
}