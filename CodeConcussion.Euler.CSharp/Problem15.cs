﻿using Math = CodeConcussion.Euler.CSharp.Utility.Math;

namespace CodeConcussion.Euler.CSharp
{
    public sealed class Problem15 : IProblem<long>
    {
        public string Description =>
            @"Starting in the top left corner of a 2×2 grid, and only being able to move to
            the right and down, there are exactly 6 routes to the bottom right corner.

            How many such routes are there through a 20×20 grid?";

        public long Answer => 137846528820;

        public long CalculatedAnswer { get; private set; }

        public void Solve()
        {
            const int width = 20;
            const int height = 20;
            const int min = width < height ? width : height;
            CalculatedAnswer = Math.Combination(width + height, min);
        }
    }
}