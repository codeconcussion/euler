﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace CodeConcussion.Euler.CSharp.Utility
{
    internal static class Math
    {
        public static long Combination(int n, int k)
        {
            //equivalent of n! / (k! * (n - k)!) or n! / (n - k)! / k!. divide as we go to avoid overflow.
            if (k > n) return 0;

            var answer = 1L;
            for (var x = 1L; x <= k; x++)
            {
                answer *= n--;
                answer /= x;
            }

            return answer;
        }

        public static IEnumerable<long> Factors(long value)
        {
            yield return 1;

            var min = (int)System.Math.Floor(System.Math.Sqrt(value));
            var factors = Enumerable.Range(2, min + 1).Where(x => value % x == 0);
            foreach (var factor in factors)
            {
                yield return factor;
                if (factor < min) yield return value / factor;
            }

            yield return value;
        }

        public static bool IsPrime(long value)
        {
            var abs = System.Math.Abs(value);
            if (abs == 2 || abs == 3) return true;
            if (abs == 1 || value % 2 == 0) return false;

            var min = (long)System.Math.Floor(System.Math.Sqrt(value));
            for (var i = 3; i <= min; i += 2)
            {
                if (value % i == 0) return false;
            }

            return true;
        }

        public static string StringAdd(string augend, string addend)
        {
            var length = augend.Length > addend.Length ? augend.Length : addend.Length;
            var a = augend.PadLeft(length, '0');
            var b = addend.PadLeft(length, '0');
            var carry = 0;
            var answer = "";

            for (var i = length - 1; i >= 0; i--)
            {
                var x = (int)char.GetNumericValue(a[i]);
                var y = (int)char.GetNumericValue(b[i]);
                var z = x + y + carry;
                carry = 0;

                if (z > 9 && i > 0)
                {
                    z -= 10;
                    carry = 1;
                }

                answer = z + answer;
            }

            return answer;
        }

        public static string StringFactorial(int number)
        {
            return Enumerable
                .Range(2, number - 1)
                .Aggregate("1", (x, y) => StringMultiply(x, y.ToString(CultureInfo.InvariantCulture)));
        }

        public static string StringMultiply(string multiplicand, string multiplier)
        {
            var answer = "";
            var carry = 0;

            for (var i = multiplier.Length - 1; i >= 0; i--)
            {
                var x = (int)char.GetNumericValue(multiplier[i]);
                var current = "".PadLeft(multiplier.Length - i - 1, '0');

                for (var j = multiplicand.Length - 1; j >= 0; j--)
                {
                    var y = (int)char.GetNumericValue(multiplicand[j]);
                    var z = x * y + carry;
                    carry = 0;

                    if (z > 9 && j > 0)
                    {
                        carry = z / 10;
                        z = z % 10;
                    }

                    current = z + current;
                }

                answer = Utility.Math.StringAdd(answer, current);
            }

            return answer;
        }
    }
}