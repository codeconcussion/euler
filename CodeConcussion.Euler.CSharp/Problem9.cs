﻿using System;

namespace CodeConcussion.Euler.CSharp
{
    public sealed class Problem9 : IProblem<long>
    {
        public string Description =>
            @"A pythagorean triplet is, a^2 + b^2 = c^2, where a, b, and c are natural numbers.
            There is one pythagorean triplet where a + b + c = 1000.
            Find the product of a, b, and c.";

        public long Answer => 31875000;

        public long CalculatedAnswer { get; private set; }
        
        public void Solve()
        {
            const int targetSum = 1000;
            var triplet = FindTriplet(targetSum);
            CalculatedAnswer = triplet.Item1 * triplet.Item2 * triplet.Item3;
        }

        private static Tuple<int, int, int> FindTriplet(int targetSum)
        {
            var upperBound = targetSum / 2;

            for (var a = 1; a <= upperBound; a++)
            for (var b = (a + 1); b <= upperBound; b++)
            {  
                var check = (a * a) + (b * b);
                var c = (int)Math.Sqrt(check); //?!:precision issues?
                if (check == (c * c) && (a + b + c) == targetSum) return Tuple.Create(a, b, c);
            }

            return Tuple.Create(0, 0, 0);
        }
    }
}
