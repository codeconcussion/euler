﻿namespace CodeConcussion.Euler.CSharp
{
    public sealed class Problem10 : IProblem<long>
    {
        public string Description => "Find the sum of all the primes below two million.";

        public long Answer => 142913828922;

        public long CalculatedAnswer { get; private set; }
        
        public void Solve()
        {
            const int max = 2000000;
            var answer = 0L;

            for (var i = 2; i < max; i++)
            {
                if (Utility.Math.IsPrime(i)) answer += i;
            }

            CalculatedAnswer = answer;
        }
    }
}