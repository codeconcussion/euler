﻿using System.Linq;

namespace CodeConcussion.Euler.CSharp
{
    public sealed class Problem3 : IProblem<long>
    {
        public string Description => "What is the largest prime factor of the number 600851475143?";

        public long Answer => 6857;

        public long CalculatedAnswer { get; private set; }

        public void Solve()
        {
            const long target = 600851475143;
            CalculatedAnswer = Utility.Math.Factors(target).Where(Utility.Math.IsPrime).Max();
        }
    }
}