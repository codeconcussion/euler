﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CodeConcussion.Euler.CSharp
{
    public sealed class Problem6 : IProblem<long>
    {
        public string Description => "Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.";

        public long Answer => 25164150;

        public long CalculatedAnswer { get; private set; }

        public void Solve()
        {
            var range = Enumerable.Range(1, 100).ToList();
            CalculatedAnswer = SquareOfSum(range) - SumOfSquares(range);
        }

        private static long SumOfSquares(IEnumerable<int> range)
        {
            return range.Select(x => (long)Math.Pow(x, 2)).Sum();
        }

        private static long SquareOfSum(IEnumerable<int> range)
        {
            return (long)Math.Pow(range.Sum(), 2);
        }
    }
}