﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CodeConcussion.Euler.CSharp.Test
{
    [TestClass]
    public sealed class Harness
    {
        [TestMethod] public void Problem1() { Solve(new Problem1()); }
        [TestMethod] public void Problem2() { Solve(new Problem2()); }
        [TestMethod] public void Problem3() { Solve(new Problem3()); }
        [TestMethod] public void Problem4() { Solve(new Problem4()); }
        [TestMethod] public void Problem5() { Solve(new Problem5()); }
        [TestMethod] public void Problem6() { Solve(new Problem6()); }
        [TestMethod] public void Problem7() { Solve(new Problem7()); }
        [TestMethod] public void Problem8() { Solve(new Problem8()); }
        [TestMethod] public void Problem9() { Solve(new Problem9()); }
        [TestMethod] public void Problem10() { Solve(new Problem10()); }
        [TestMethod] public void Problem11() { Solve(new Problem11()); }
        [TestMethod] public void Problem12() { Solve(new Problem12()); }
        [TestMethod] public void Problem13() { Solve(new Problem13()); }
        [TestMethod] public void Problem14() { Solve(new Problem14()); }
        [TestMethod] public void Problem15() { Solve(new Problem15()); }
        [TestMethod] public void Problem16() { Solve(new Problem16()); }
        [TestMethod] public void Problem17() { Solve(new Problem17()); }
        [TestMethod] public void Problem18() { Solve(new Problem18()); }
        [TestMethod] public void Problem19() { Solve(new Problem19()); }
        [TestMethod] public void Problem20() { Solve(new Problem20()); }
        [TestMethod] public void Problem29() { Solve(new Problem29());}
        [TestMethod] public void Problem31() { Solve(new Problem31()); }
        [TestMethod] public void Problem35() { Solve(new Problem35()); }
        [TestMethod] public void Problem49() { Solve(new Problem49()); }
        [TestMethod] public void Problem54() { Solve(new Problem54()); }
        [TestMethod] public void Problem67() { Solve(new Problem67()); }

        private static void Solve<T>(IProblem<T> problem)
        {
            Console.WriteLine(problem.Description);

            problem.Solve();
            Console.WriteLine("Answer: " + problem.Answer);
            Console.WriteLine("Calculated Answer: " + problem.CalculatedAnswer);
            Assert.AreEqual(problem.Answer, problem.CalculatedAnswer);
        }
    }
}