﻿module Problem2
//By considering the terms in the Fibonacci sequence whose values do not exceed
//four million, find the sum of the even-valued terms.

let answer = 4613732
let max = 4000000

let rec fibonacci (acc : List<int>) =
    let next = acc.[0] + acc.[1]
    if next < max then
        fibonacci (next :: acc)
    else
        acc

let solve =
    fibonacci [2;1]
    |> List.filter (fun x -> x % 2 = 0)
    |> List.sum

printf "%i" solve