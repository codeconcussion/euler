﻿module Problem3
//What is the largest prime factor of the number 600851475143?

let answer = 6857
let target = 600851475143L

let factorsOf a =
    [
        yield 1L
        yield! List.filter (fun x -> a % x = 0L) [3L .. 2L .. int64 (sqrt(float a))]
        yield a
    ]

let isPrime x =
    (List.length (factorsOf x)) = 2

let primeFactors =
    factorsOf target |> List.filter isPrime

let solve =
    primeFactors.[primeFactors.Length - 1]

printf "%i" solve