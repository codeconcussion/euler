﻿module Problem1
//Find the sum of all the multiples of 3 or 5 below 1000.

let answer = 233168
let max = 999

let multiplesOf x =
    [x .. x .. max]

let multiples =
    (multiplesOf 3) @ (multiplesOf 5)
    |> Seq.distinct

let solve =
    multiples |> Seq.sum

printf "%i" solve